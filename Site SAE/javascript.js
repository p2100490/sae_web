function ScrollTo(name) {
  ScrollToResolver(document.getElementById(name));
}

function ScrollToResolver(elem) {
  var jump = parseInt(elem.getBoundingClientRect().top * 0.1);
  document.body.scrollTop += jump;
  document.documentElement.scrollTop += jump;
  if (!elem.lastjump  || elem.lastjump > Math.abs(jump)){
    elem.lastjump = Math.abs(jump) ;
    setTimeout(function() { ScrollToResolver(elem);}, "10");
  } else {
    elem.lastjump = null;
  }
}